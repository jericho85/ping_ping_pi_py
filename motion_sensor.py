import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3,GPIO.OUT)
GPIO.setup(11,GPIO.IN)

#################################################
#                                               #
# Configuration                                 #
#                                               #
# Update these variables to control the         #
# functionatility of the script                 #
#                                               #
#################################################


# STATUS is the output variable
# status = output variable: 1 = table in use, 0 = table available
status = 0

# the % of positive observations in the period indicating table in use
percent_motion = 1/3

# A period is the number of observations specified here
period = 60

#################################################

activity = []                                   # creating an array to store recent actions

while True:
    i = GPIO.input(11)                          # get the state of GPIO pin #11
    if i > 0:                                   # pin status == 0 === no activity, > 0 === activity
        activity.append(1)
        i = 1
    else:
        activity.append(0)
        i = 0
    if len(activity) > period:                  # limit array size to period
        for i in range(0,len(activity)-period): 
            del activity[0]
    if sum(activity)> (percent_motion*period):  # check actual usage against specified ratio
        status = 1                              # update availability accordingly
    else:
        status = 0
    print(i,status,len(activity),sum(activity))
    print(activity)
    time.sleep(1)

